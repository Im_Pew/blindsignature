package hr.dturic.faculty.main;

import hr.dturic.faculty.signature.algorithm.Signer;

import java.math.BigInteger;
import java.security.interfaces.RSAPublicKey;

public class MainClass {

    private Signer signer;

    private BigInteger mask;

    public static void main(String[] args) {

        new MainClass().blindSignatureBigInteger();
    }

    public void blindSignatureBigInteger() {

        signer = new Signer();

        RSAPublicKey key = (RSAPublicKey) signer.getPublicKey();

        do {
            mask = BigInteger.valueOf((long) (Math.random() * 1000));
        } while (!mask.gcd(key.getModulus()).equals(BigInteger.ONE));

        long req = (long) (Math.random() * Long.MAX_VALUE);


        BigInteger request = new BigInteger(String.valueOf(req));
        System.out.println("Zahtijev: " + request.toString(16));

        // m' = m*r^e (mod N)
        request = request.multiply(mask.modPow(key.getPublicExponent(), key.getModulus())).mod(key.getModulus());

        System.out.println("Maska: " + mask.toString(16));
        System.out.println("Zamaskirani zahtjev: " + request.toString(16));

        // s' = (m')^d (mod N)
        BigInteger response = signer.sign(request);

        System.out.println("Zamaskirani potpis: " + response.toString(16));

        // s = s' * r^-1 (mod N)
        response = response.multiply(mask.modInverse(key.getModulus())).mod(key.getModulus());

        System.out.println("Odmaskirani potpis: " + response.toString(16));

        System.out.println("Provjera potpisa: " + signer.encrypt(response).toString(16));
    }
}
