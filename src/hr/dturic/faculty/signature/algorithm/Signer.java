package hr.dturic.faculty.signature.algorithm;

import java.math.BigInteger;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;

public class Signer {

    public Signer() {
        generateKeys();
    }

    private KeyPair keyPair;

    private void generateKeys() {
        try {
            KeyPairGenerator kpg = KeyPairGenerator.getInstance("RSA");

            kpg.initialize(512);
            keyPair = kpg.generateKeyPair();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

    }

    public PublicKey getPublicKey() {

        return keyPair.getPublic();
    }

    public BigInteger encrypt(BigInteger number) {

        RSAPublicKey key = (RSAPublicKey) keyPair.getPublic();

        return number.modPow(key.getPublicExponent(), key.getModulus());
    }

    public BigInteger sign(BigInteger message) {

        RSAPrivateKey key = (RSAPrivateKey) keyPair.getPrivate();

        return message.modPow(key.getPrivateExponent(), key.getModulus());
    }

    public BigInteger decrypt(BigInteger msg) {

        RSAPrivateKey key = (RSAPrivateKey) keyPair.getPrivate();

        return msg.modPow(key.getPrivateExponent(), key.getModulus());
    }
}
